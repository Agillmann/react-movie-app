import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import {MemoryRouter} from 'react-router';
import Nav from './NavContainer';

storiesOf('Components', module)
.addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
))
  .add('Nav', () => (
    <Nav/>
  ));