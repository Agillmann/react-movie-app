import React from 'react';
import CardMovie from '../../CardMovie/CardMovieContainer';
import Nav from '../../Nav/NavContainer';
import styled from '@emotion/styled';

var white = "#FFFFFF";
var red = "#C03A2B";

const TextTitle = styled.h2`
  color: ${red};
  font-family: NetflixBold;
  font-size: 64px;
`

const TextSection= styled.h1`
  color: white;
  font-family: NetflixBold;
  font-size: 64px;
  background: black;
`

const HomeContainer = styled.div`
  background-color:${white};
`

const Home = ({dataPopular, dataBoxOffice, dataTrendingShows, dataPopularShows}) => (
  <HomeContainer className="home">
    <Nav/> 
    <div className="row">
      <TextSection className="col-md-12 center-md" key="1">
          Movies
      </TextSection>
      <TextTitle className="col-md-12 center-md" key="2">
        Box-Office
      </TextTitle>
      {dataBoxOffice.length > 0 && (
        dataBoxOffice.map(movie => (
          <div className="col-md-4 center-md" key={movie.imdbID}>
            <CardMovie
              data={movie}
              id={movie.imdbID}
              key={movie.imdbID}
              poster={movie.Poster}
              title={movie.Title}
              rating={movie.imdbRating}
              genre={movie.Genre}
            />
          </div>
        ))
      )}
      </div>
    <div className="row">
    <TextTitle className="col-md-12 center-md" key="3">
        Popular
    </TextTitle>
      {dataPopular.length > 0 && (
          dataPopular.map(movie => (
            <div className="col-md-3 center-md" key={movie.imdbID}>
              <CardMovie
                data={movie}
                id={movie.imdbID}
                key={movie.imdbID}
                poster={movie.Poster}
                title={movie.Title}
                rating={movie.imdbRating}
                genre={movie.Genre}
              />
            </div>
          ))
        )}
    </div>
    <div className="row">
      <TextSection className="col-md-12 center-md" key="4">
            Shows
      </TextSection>
      <TextTitle className="col-md-12 center-md" key="5">
        Trending
      </TextTitle>
        {dataTrendingShows.length > 0 && (
            dataTrendingShows.map(show => (
              <div className="col-md-3 center-md" key={show.imdbID}>
                <CardMovie
                  data={show}
                  id={show.imdbID}
                  key={show.imdbID}
                  poster={show.Poster}
                  title={show.Title}
                  rating={show.imdbRating}
                  genre={show.Genre}
                />
              </div>
            ))
          )}
      <TextTitle className="col-md-12 center-md" key="6">
        Popular
      </TextTitle>
      {dataPopularShows.length > 0 && (
            dataPopularShows.map(show => (
              <div className="col-md-3 center-md" key={show.imdbID}>
                <CardMovie
                  data={show}
                  id={show.imdbID}
                  key={show.imdbID}
                  poster={show.Poster}
                  title={show.Title}
                  rating={show.imdbRating}
                  genre={show.Genre}
                />
              </div>
            ))
          )}
      
    </div>
  </HomeContainer>
);

export default Home;
