import React, { Component } from 'react';
import Home from './Home';
import api from '../../../helpers/api';

class HomeContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataBoxOffice: [],
            dataPopular: [],
            dataTrendingShows: [],
            dataPopularShows: [],
        };
    }

    async componentDidMount() {
        let dataTrakt01 = await api.getBoxOffice();
        let dataB = [];
        for (let i = 0; i < 6; i++) {
            let dataOmdb = await api.getMovieById(dataTrakt01[i].movie.ids.imdb);
            dataB.push(dataOmdb);
        }
        this.setState({dataBoxOffice: dataB})
        //console.log(dataM);
        //console.log(dataOmdb);
        //console.log(dataTrakt[0]);
        //console.log(dataTrakt[0].movie.ids.imdb);

        let dataTrakt02 = await api.getPopular();
        let dataP = [];
        for (let i = 0; i < 10; i++) {
            let dataOmdb = await api.getMovieById(dataTrakt02[i].ids.imdb);
            dataP.push(dataOmdb);
        }
        this.setState({dataPopular: dataP})

        let dataTrakt03 = await api.getPopularShows();
        let dataS1 = [];
        for (let i = 0; i < 10; i++) {
            let dataOmdb = await api.getMovieById(dataTrakt03[i].ids.imdb);
            dataS1.push(dataOmdb);
        }
        this.setState({dataPopularShows: dataS1})

        let dataTrakt04 = await api.getTrendingShows();
        let dataS2 = [];
        for (let i = 0; i < 10; i++) {
            let dataOmdb = await api.getMovieById(dataTrakt04[i].show.ids.imdb);
            dataS2.push(dataOmdb);
        }
        this.setState({dataTrendingShows: dataS2})
    }

    render() {
        console.log(this.state);
        return (
            <Home
                key="1"
                handleClick={this.handleClick}
                dataBoxOffice={this.state.dataBoxOffice}
                dataPopular={this.state.dataPopular}
                dataPopularShows={this.state.dataPopularShows}
                dataTrendingShows={this.state.dataTrendingShows}
            />
        );
    }
}

export default HomeContainer;
