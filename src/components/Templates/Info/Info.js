import React from 'react';
import Nav from '../../Nav/NavContainer';
import styled from '@emotion/styled';

var white = "#FFFFFF";

const TextTitle = styled.h2`
  font-family: NetflixBold;
  font-size: 64px;
`
const Paragraphe = styled.div`
  font-size: 20px;
`
const Span = styled.span`
  font-family: NetflixBold;
`

const Plot = styled.p`
  font-size: 22px;
`

const ButtonLike = styled.button`
  background:${white};
  border: none;
  cursor:pointer;
`
const ButtonWatched = styled.button`
  background:${white};
  border: none;
  cursor:pointer;
`
const IconLike = styled.i`
  font-size:34px;
`
const IconWatched = styled.i`
  font-size:34px;
`

const Info = ({data,handleClickLike, handleClickWatched, colorLike, colorWatched}) => (
  <div className="home">
    <Nav/>
    <div className="row">
      <div className="col-md-12 center-md">
        <TextTitle className="">
          {data.Title}
        </TextTitle>
      </div>
      <div className="col-md-offset-1 col-md-2">
        <img src={data.Poster} alt="poster"/>
      </div>
      <Paragraphe className="col-md-8">
        <Plot>{data.Plot}</Plot>
        <p><Span>Realease :</Span> {data.Released}</p>
        <p><Span>Director :</Span> {data.Director}</p>
        <p><Span>Prodution :</Span> {data.Production}</p>
        <p><Span>Writer :</Span> {data.Writer}</p>
        <p><Span>Stars :</Span> {data.Actors}</p>
        <p><Span>Country :</Span> {data.Country}</p>
        <p><Span>Genre :</Span> {data.Genre}</p>
        <p><Span>Rating :</Span> {data.imdbRating}/10</p>
      </Paragraphe>
    </div>    
    <div className="row">
      <ButtonLike onClick={handleClickLike} className="col-md-offset-5 col-md-1" style={{color:colorLike}}>
        <IconLike className="fas fa-heart"/>
      </ButtonLike>
      <ButtonWatched onClick={handleClickWatched} className="col-md-1" style={{color:colorWatched}}>
        <IconWatched className="fas fa-eye"/>
      </ButtonWatched>
    </div>
    
  </div>
);

export default Info;
