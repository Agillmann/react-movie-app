import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import {MemoryRouter} from 'react-router';
import Info from './InfoContainer';

storiesOf('Templates', module)
.addDecorator(story => (
  <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
))
.add('Info', () => (
  <Info

  />
));