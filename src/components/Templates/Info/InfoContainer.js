import React, { Component } from 'react';
import Info from './Info';
import api from '../../../helpers/api';
import localstorage from '../../../helpers/localstorage';

class InfoContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            id: "",
            like: false,
            colorLike: "grey",
            watched: false,
            colorWatched: "grey"
        };
    }

    async componentDidMount() {
        const data = await api.getMovieById(this.props.match.params.id);
        this.setState({
            data: data,
            id: this.props.match.params.id
        })
        //console.log(data);
    }
    async componentDidUpdate() {
        if(this.props.match.params.id !== this.state.id){
            const data = await api.getMovieById(this.props.match.params.id);
            this.setState({
                data: data,
                id: this.props.match.params.id
            });
        }
    }

    handleClickLike = (e) => {
        e.preventDefault();
        if(!this.state.like){
            localstorage.addToLocalStorage('likeList',this.state.data);
            console.log('LIKE LIST :',localstorage.getLikeList());
            this.setState({
                colorLike: "#C03A2B",
                like: true
            });
        }
        else {
            localstorage.deleteFromLocalStorage('likeList',this.state.data);
            console.log('LIKE LIST :',localstorage.getLikeList());
            this.setState({
                colorLike: "grey",
                like: false
            });
        };
    };

    handleClickWatched = (e) => {
        e.preventDefault();
        if(!this.state.watched){
            localstorage.addToLocalStorage('watchedList',this.state.data);
            console.log('WATCHED LIST :',localstorage.getWatchedList());
            this.setState({
                colorWatched: "#014CB7",
                watched: true
            });
        }
        else {
            localstorage.deleteFromLocalStorage('watchedList',this.state.data);
            console.log('WATCHED LIST :',localstorage.getWatchedList());
            this.setState({
                colorWatched: "grey",
                watched: false
            });
        }
    };

    render() {
        console.log(this.state.data)
        return (
            <Info
                handleClickLike={this.handleClickLike}
                handleClickWatched={this.handleClickWatched}
                colorLike={this.state.colorLike}
                colorWatched={this.state.colorWatched}
                data={this.state.data}
            />
        );
    }
}

export default InfoContainer;
