import React from 'react';
import Nav from '../../Nav/NavContainer';
import CardMovie from '../../CardMovie/CardMovieContainer';
import styled from '@emotion/styled';

var red = "#C03A2B";
var blue = '#014CB7';


const ListContainer = styled.div`
  padding: 0px 20px;
`

const TextLike = styled.h2`
  color: ${red};
  font-family: NetflixBold;
  font-size: 64px;
`

const TextWatched = styled.h2`
  color: ${blue};
  font-family: NetflixBold;
  font-size: 64px;
`

const List = ({dataLike, dataWatched}) => (
  <div className="home">
    <Nav/>
    <ListContainer className="row">
      <TextLike className="col-md-12 center-md">
        Like
      </TextLike>
      {dataLike ? (
        dataLike.map(movie => (
          <div className="col-md-2 center-md" key={movie.imdbID}>
            <CardMovie
              data={movie}
              id={movie.imdbID}
              key={movie.imdbID}
              poster={movie.Poster}
              title={movie.Title}
              rating={movie.imdbRating}
              genre={movie.Genre}
            />
          </div>
        ))
      ) : "empty"}
      </ListContainer>
    <ListContainer className="row">
    <TextWatched className="col-md-12 center-md">
        Watched
    </TextWatched>
      {dataWatched ? (
        dataWatched.map(movie => (
          <div className="col-md-2 center-md" key={movie.imdbID}>
            <CardMovie
              data={movie}
              id={movie.imdbID}
              key={movie.imdbID}
              poster={movie.Poster}
              title={movie.Title}
              rating={movie.imdbRating}
              genre={movie.Genre}
            />
          </div>
          ))
        ) : "empty"}
    </ListContainer>
    
  </div>
);

export default List;
