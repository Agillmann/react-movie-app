import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import {MemoryRouter} from 'react-router';
import List from './ListContainer';

storiesOf('Templates', module)
.addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
))
.add('List', () => (
    <List
    />
));