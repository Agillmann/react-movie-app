import React, { Component } from 'react';
import List from './List';
import localstorage from '../../../helpers/localstorage';

class ListContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataLike: [],
            dataWatched: [],
        };
    }

    async componentDidMount() {
        this.setState({
            dataLike: localstorage.getLikeList(),
            dataWatched: localstorage.getWatchedList()
        })
    }

    // async componentDidUpdate(prevProps, prevState) {
    //     // console.log(localstorage.getLikeList());
    //     // console.log('prev :',prevState.dataLike.length)
    //     // console.log(this.state.dataLike.length)
    //     if(prevState.dataLike.length !== this.state.dataLike.length){
    //         this.setState({
    //             dataLike: localstorage.getLikeList(),
    //             dataWatched: localstorage.getWatchedList()
    //         })
    //     }
    // }
    
    render() {
        console.log(this.state)
        return (
            <List
                dataLike={this.state.dataLike}
                dataWatched={this.state.dataWatched}
            />
        );
    }
}

export default ListContainer;
