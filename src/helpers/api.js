const TRAKT_API_KEY = process.env.TRAKT_API_KEY || "Your Trakt api key";
const OMDB_API_KEY = process.env.OMDB_API_KEY || "Your OMDB api key";

const OMDB_API_URI = "http://www.omdbapi.com/"; // ex : http://www.omdbapi.com/?i=tt3896198&apikey=a3544
const TRAKT_API_BOXOFFICE = "https://api.trakt.tv/movies/boxoffice";
const TRAKT_API_POPULAR = "https://api.trakt.tv/movies/popular";
const TRAKT_API_TRENDING_SHOWS = "https://api.trakt.tv/shows/trending";
const TRAKT_API_POPULAR_SHOWS = "https://api.trakt.tv/shows/popular";

/*
SET HEADERS FOR TRAKT API
Content-Type:application/json
trakt-api-version:2
trakt-api-key:[client_id]
*/

class api {

    /////////////////////////// TRAKT API MOVIE
    async getBoxOffice() {
        const options = {
            method: 'get',
            headers: {
                'Content-type': 'application/json',
                'trakt-api-version':'2',
                'trakt-api-key': TRAKT_API_KEY
            }
        }
        const response = await fetch(TRAKT_API_BOXOFFICE, options);
        const json = await response.json();
        return json;
    }

    async getPopular() {
        const options = {
            method: 'get',
            headers: {
                'Content-type': 'application/json',
                'trakt-api-version':'2',
                'trakt-api-key': TRAKT_API_KEY
            }
        }
        const response = await fetch(TRAKT_API_POPULAR, options);
        const json = await response.json();
        return json;
    };

    /////////////////////////// TRAKT API SHOWS
    async getPopularShows() {
        const options = {
            method: 'get',
            headers: {
                'Content-type': 'application/json',
                'trakt-api-version':'2',
                'trakt-api-key': TRAKT_API_KEY
            }
        }
        const response = await fetch(TRAKT_API_POPULAR_SHOWS, options);
        const json = await response.json();
        return json;
    };

    async getTrendingShows() {
        const options = {
            method: 'get',
            headers: {
                'Content-type': 'application/json',
                'trakt-api-version':'2',
                'trakt-api-key': TRAKT_API_KEY
            }
        }
        const response = await fetch(TRAKT_API_TRENDING_SHOWS, options);
        const json = await response.json();
        return json;
    };

    /////////////////////////// OMDB API
    async getMovieById(idM) {
        var omdburl = OMDB_API_URI;
        omdburl += "?i=" + idM ;
        omdburl += "&apikey=" + OMDB_API_KEY;

        const response = await fetch(omdburl);
        const json = await response.json();
        return json;
        }

    async getMovieByTitle(title) {
        var omdburl = OMDB_API_URI;
        omdburl += "?t=" + title;
        omdburl += "&apikey=" + OMDB_API_KEY;

        const response = await fetch(omdburl);
        const json = await response.json();
        return json;
        }
}

export default new api();
  